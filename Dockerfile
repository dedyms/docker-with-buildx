FROM alpine AS fetcher
RUN apk add curl
ARG TARGETARCH
ARG BUILDX_VERSION
RUN curl -L \
  --output /docker-buildx \
  "https://github.com/docker/buildx/releases/download/v$BUILDX_VERSION/buildx-v$BUILDX_VERSION.linux-$TARGETARCH"
RUN chmod a+x /docker-buildx

FROM docker:latest
COPY --from=fetcher /docker-buildx /usr/lib/docker/cli-plugins/docker-buildx

